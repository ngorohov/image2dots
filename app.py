from PIL import Image
from random import randint
import json
import sys
import getopt

__author__ = 'gorohov'

def main(argv):
    image = ''
    out = 'output.png'
    jsonfile = 'data.json'
    try:
        opts, args = getopt.getopt(argv, "i:o:j:")
    except getopt.GetoptError:
        print('Usage: python3 app.py -i <imagename> -o <outimagename> -j <json>')
        sys.exit(2)
    for opt, arg in opts:
        if arg == "-h":
            print('Usage: python3 app.py -i <imagename> -o <outimagename> -j <json>')
            sys.exit(0)
        elif opt  == "-i":
            image = arg
        elif opt  == "-o":
            out = arg
        elif opt == "-j":
            jsonfile = arg
    processimage(image, out, jsonfile)

def processimage(image, out, jsonfile):
    try:
        im = Image.open(image)
    except (FileNotFoundError, OSError):
        print('File %s not found or not an image: ' % (image))
        exit(2)
    width, height = im.size
    outimg = Image.new('RGB', (width, height), "white")
    rgb_im = im.convert('RGB')
    totalcount = 0
    data = []

    for y in range(1, height):
        for x in range(1, width):
            r, g, b = rgb_im.getpixel((x, y))
            # print(r, g, b)
            if  randint(1,10) < 2 and r < 10 and g < 10 and b < 10:
                outimg.putpixel((x, y), (0, 0, 0, 1))
                totalcount += 1
                data.append([x, y])
            else:
                outimg.putpixel((x, y), (255, 255, 255, 1))

    with open(jsonfile, 'w') as outfile:
        json.dump(data, outfile)
    outimg.save(out)
    print('Total dots: ', totalcount)


if __name__ == '__main__':
  main(sys.argv[1:])
    
