Usage:

```
#!bash

python3 app.py -i <inputImage> -o <outputImage> -j <outputJson>
```